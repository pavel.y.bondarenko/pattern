package ua.bondarenko.decorator;

public class CarWashService extends OptionDecorator {
    public CarWashService(Service service) {
        super(service, 1000, "Car wash!");
    }
}
