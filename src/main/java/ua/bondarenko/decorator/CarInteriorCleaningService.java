package ua.bondarenko.decorator;

public class CarInteriorCleaningService extends OptionDecorator {

    public CarInteriorCleaningService(Service service) {
        super(service, 1500, "Car interior cleaning!");
    }
}
