package ua.bondarenko.decorator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {
    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        Service carService = new CarService(1000, "Car diagnostic!");
        Service totalService = new CarInteriorCleaningService(new CarWashService(carService));

        printLabelAndPrice(totalService);
    }

    private static void printLabelAndPrice(Service service) {
        LOGGER.info(service.getLabel());
        LOGGER.info("Total price = {}", service.getPrice());
    }
}
