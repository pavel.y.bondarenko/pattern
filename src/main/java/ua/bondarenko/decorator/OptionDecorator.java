package ua.bondarenko.decorator;

public abstract class OptionDecorator implements Service {
    private final Service service;
    private final double price;
    private final String label;

    protected OptionDecorator(Service service, double price, String label) {
        this.service = service;
        this.price = price;
        this.label = label;
    }


    @Override
    public double getPrice() {
        return service.getPrice() + this.price;
    }

    @Override
    public String getLabel() {
        return service.getLabel() + " " + this.label;
    }
}
