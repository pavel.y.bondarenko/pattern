package ua.bondarenko.decorator;

public interface Service {
    double getPrice();
    String getLabel();
}
