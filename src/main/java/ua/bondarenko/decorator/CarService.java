package ua.bondarenko.decorator;

public class CarService implements Service {

    private final double price;
    private final String label;

    public CarService(double price, String label) {
        this.price = price;
        this.label = label;
    }

    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public String getLabel() {
        return label;
    }
}
