package ua.bondarenko.adapter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

public class Main {
    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {

        Enumeration<String> enumeration = Collections.enumeration(List.of("one", "two", "three"));

        printByLogger(new IterableAdapter<>(enumeration));
    }

    private static void printByLogger(Iterable<String> iterable) {
        for (String element : iterable) {
            LOGGER.info(element);
        }
    }
}
