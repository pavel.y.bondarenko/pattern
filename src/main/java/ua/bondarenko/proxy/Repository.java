package ua.bondarenko.proxy;

public interface Repository<T> {
    T findById(long id);
}
