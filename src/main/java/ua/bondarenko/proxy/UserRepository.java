package ua.bondarenko.proxy;

import java.util.List;

public class UserRepository implements Repository<User> {
    private final List<User> users =
            List.of(
                    new User(1, "Ben"),
                    new User(2, "Anna"),
                    new User(3, "Piter"));

    @Override
    public User findById(long id) {
        return users.stream()
                .filter(user -> user.getId() == id)
                .findFirst()
                .get();
    }
}
