package ua.bondarenko.proxy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {
    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        LOGGER.info("Start UserRepository! ----------------------------------------");
        useRepository(new UserRepository());
        LOGGER.info("Start user repository proxy!----------------------------------");
        useRepository(new UserRepositoryProxy());
    }

    private static void useRepository(Repository<User> repository) {
        LOGGER.info(repository.findById(1).toString());
        LOGGER.info(repository.findById(1).toString());
        LOGGER.info(repository.findById(2).toString());
    }
}
