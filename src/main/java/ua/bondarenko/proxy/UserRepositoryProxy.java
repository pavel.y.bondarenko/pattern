package ua.bondarenko.proxy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class UserRepositoryProxy implements Repository<User> {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserRepositoryProxy.class);

    private UserRepository repository;
    private final Map<Long, User> users = new HashMap<>();

    @Override
    public User findById(long id) {

        if (repository == null) {
            repository = new UserRepository();
            LOGGER.debug("User repository created!");
        }

        if (!users.containsKey(id)) {
            users.put(id, repository.findById(id));
            LOGGER.debug("New user with id = {} added in cash", id);
        }

        return users.get(id);
    }
}
